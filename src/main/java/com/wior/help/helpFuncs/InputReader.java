package com.wior.help.helpFuncs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class InputReader<R> {

    private final ScannerConsole consoleInputReader;
    private final RetryHandler retryReader;

    @Autowired
    protected InputReader(ScannerConsole consoleInputReader, RetryHandler retryReader) {
        this.consoleInputReader = consoleInputReader;
        this.retryReader = retryReader;
    }

    public R read() {
        boolean requestRetry = true;

        while (requestRetry) {
            try {
                final String userInput = consoleInputReader.read();
                return readInput(userInput);
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong input");
                requestRetry = retryReader.isRetryRequired();
            }
        }

        throw new IllegalArgumentException("User didn't enter necessary input.");
    }

    abstract R readInput(String userInput) throws IllegalArgumentException;
}
