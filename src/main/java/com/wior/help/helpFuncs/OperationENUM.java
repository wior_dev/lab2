package com.wior.help.helpFuncs;

import com.wior.help.operations.MathOperator;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class OperationENUM extends InputReader<MathOperator> {

    private final Map<String, MathOperator> operatorMap = ImmutableMap.<String, MathOperator>builder()
            .put("+", MathOperator.ADDITION)
            .put("-", MathOperator.SUBTRACTION)
            .put("/", MathOperator.DIVISION)
            .put("*", MathOperator.MULTIPLICATION)
            .build();

    OperationENUM(ScannerConsole consoleInputReader, RetryHandler retryReader) {
        super(consoleInputReader, retryReader);
    }

    @Override
    MathOperator readInput(String userInput) {
        return Optional.ofNullable(operatorMap.get(userInput)).orElseThrow(IllegalArgumentException::new);
    }
}
