
package com.wior.help.helpFuncs;

import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
class ScannerConsole {

    private final Scanner scanner = new Scanner(System.in);

    String read(){
        return scanner.nextLine();
    }
}