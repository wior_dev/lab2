package com.wior.help.operations;


public interface MathOperation {

    double operate(double leftArgument, double rightArgument);
}
