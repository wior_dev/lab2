package com.wior.help.operations;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class OperationFactory {

    private final ImmutableMap<MathOperator, Class<? extends MathOperation>> operationsMap =
            ImmutableMap.<MathOperator, Class<? extends MathOperation>>builder()
                    .put(MathOperator.ADDITION, Add.class)
                    .put(MathOperator.SUBTRACTION, SubtrationOperation.class)
                    .put(MathOperator.DIVISION, Division.class)
                    .put(MathOperator.MULTIPLICATION, Multiplicate.class)
                    .build();

    private final ApplicationContext applicationContext;

    @Autowired
    public OperationFactory(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public MathOperation getMathOperation(MathOperator mathOperator) {
        final Class<? extends MathOperation> operationClass = operationsMap.get(mathOperator);

        if (Objects.isNull(operationClass)) {
            throw new RuntimeException(String.format("%s that is Wrong operator", mathOperator));
        }

        return applicationContext.getBean(operationClass);
    }
}
