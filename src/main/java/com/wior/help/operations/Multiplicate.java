package com.wior.help.operations;

import org.springframework.stereotype.Service;

@Service
public class Multiplicate implements MathOperation {

    public double operate(double first, double second) {
        return first * second;
    }}
