package com.wior.help.operations;

public enum MathOperator {
    ADDITION("+"), SUBTRACTION("-"), DIVISION("/"), MULTIPLICATION("*");

    private final String value;

    MathOperator(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
