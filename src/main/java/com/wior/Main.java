package com.wior;


import com.wior.help.operations.MathOperator;
import com.wior.help.operations.OperationFactory;
import com.wior.help.helpFuncs.OperationENUM;
import com.wior.help.helpFuncs.RetryHandler;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        startFunction();
    }



    private static void startFunction(){
        final AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext("com.wior");
        Scanner scanner = new Scanner(System.in);
        final OperationFactory operationFactory = applicationContext.getBean(OperationFactory.class);
        final OperationENUM operationReader = applicationContext.getBean(OperationENUM.class);
        final RetryHandler retryReader = applicationContext.getBean(RetryHandler.class);

        boolean userExit = true;
        System.out.println("Hello $UserName");
        while (userExit){
            System.out.println("Please, Enter the first operand: ");
            final double leftArg = Double.parseDouble(scanner.nextLine());
            System.out.println("Please, Enter the second operand: ");
            final double rightArg = Double.parseDouble(scanner.nextLine());

            System.out.println("Enter operation: +,-,/,*");
            final MathOperator mathOperator = operationReader.read();
            final double result = operationFactory.getMathOperation(mathOperator).operate(leftArg, rightArg);

            System.out.println(String.format("%.2f %s %.2f = %.2f",
                    leftArg, mathOperator.getValue(), rightArg, result));
            userExit = retryReader.isRetryRequired();
        }
    }
}
